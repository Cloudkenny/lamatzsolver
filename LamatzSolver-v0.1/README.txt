The program LamatzSolver-v0.1 can be started as follows by the given OS:

Windows User:
------------ 
Can start the program with LamatzSolver-v0.1.jar in the cmd-line-interface. [For example: java -jar LamatzSolver-v0.1.jar -p EE-GR -f real_4.tgf -fo tgf ]

Linux User: 
------------ 
Can start the program with the shell script LamatzSolver-v0.1.sh. [Further information about linux are not given, for this purpose look at external sources.]
IMPORTANT: If there is any complication with this file, try to copy the code from this file to a new shell file. This could be because this file is made in windows 8.1. 

Program-Interface:
-------------------
The program interface follows the interface given by http://argumentationcompetition.org/2015/iccma15notes_v3.pdf and is implemented in java.



