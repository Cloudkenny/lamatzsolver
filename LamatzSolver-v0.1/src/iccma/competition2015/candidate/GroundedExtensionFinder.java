package iccma.competition2015.candidate;

import java.util.HashMap;

/**
 * This class finds for every abstract argumentation framework the grounded
 * extension.
 * 
 * @author Nico Lamatz
 * @version 0.1
 */

public class GroundedExtensionFinder {

	/* The grounded HashMap contains arguments for this type of extension */
	private HashMap<String, ArgumentContainer> grounded = new HashMap<String, ArgumentContainer>();

	/*
	 * Just a HashMap for working because in for-loop set changing is not
	 * possible
	 */
	private HashMap<String, ArgumentContainer> workMap = new HashMap<String, ArgumentContainer>();

	/* A HashMap for argument which are label out */
	private HashMap<String, ArgumentContainer> out = new HashMap<String, ArgumentContainer>();

	/*Parameter for comparing the size of grounded.*/
	private int prev = -1, cur = 0;

	public GroundedExtensionFinder(HashMap<String, ArgumentContainer> typeZero) {
		/*
		 * Every grounded determination depends on the typeZero-arguments-set.
		 * Is the set typeZero-arguments empty then the grounded extension is
		 * also empty.
		 */

		if (!typeZero.isEmpty()) {
			grounded.putAll(typeZero);
			build();
		}
	}

	/**
	 * Constructs the grounded extension with an iterative procedure.
	 */

	private void build() {
		/* Check the current size */
		cur = grounded.size();

		/*
		 * Determines the attacks of grounded to other arguments and then add
		 * them to the out HashMap.
		 */
		for (String name : grounded.keySet()) {
			HashMap<String, ArgumentContainer> defender = grounded.get(name)
					.getAttackOn();
			out.putAll(defender);

		}

		/*
		 * For every out, see if the next attacked are defended. If(true) than
		 * put them to workMap.
		 */
		
		for (String attack : out.keySet()) {
			ArgumentContainer attacked = out.get(attack);
			for (String defend : attacked.getAttackOn().keySet()) {
				ArgumentContainer defended = attacked.getAttackOn().get(defend);
				if (out.keySet().containsAll(defended.getAttackOf().keySet())) {
					workMap.put(defended.name, defended);
				}
			}
		}
		
		/*Add the workMap to grounded*/
		grounded.putAll(workMap);
		grounded.keySet().removeAll(out.keySet());
		
		/*Clear Out for next round*/
		out.clear();
		
		/*Clear workMap for next round*/
		workMap.clear();
		
		/*When the size of grounded changed than start next round*/
		if (prev != cur) {
			prev = cur;
			build();
		} 
	}

	/**
	 * Return the grounded extension.
	 * 
	 * @return grounded: the grounded extension.
	 */
	public HashMap<String, ArgumentContainer> getGroundedExtension() {
		return grounded;
	}
}
