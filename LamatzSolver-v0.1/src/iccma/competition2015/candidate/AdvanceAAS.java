package iccma.competition2015.candidate;

import java.util.HashMap;

/**
 * This class provides an implementation of dungs abstract framework with
 * further information. These information are:
 * 
 * TypeZero Arguments - arguments which are defended by the empty set. 
 * 
 * TypeOne Arguments - arguments which are not defended by the empty set but can defend
 * itself. 
 * 
 * TypeTwo Arguments - argument which can only defended by a set with S
 * \subset Arguments and |S| > 1. 
 * 
 * Delete Arguments - arguments which can not be
 * part of any extension because those are defeated by TypeZero arguments.
 * 
 * @author Nico Lamatz
 * @version 0.1
 */

public class AdvanceAAS implements AdvanceArgumentationFrameworkMethods {
	
	/*As explained in class description*/
	public HashMap<String, ArgumentContainer> AAS;
	private HashMap<String, ArgumentContainer> typeZero;
	private HashMap<String, ArgumentContainer> typeOne;
	private HashMap<String, ArgumentContainer> typeTwo;
	private HashMap<String, ArgumentContainer> delete = new HashMap<String, ArgumentContainer>();

	public AdvanceAAS(HashMap<String, ArgumentContainer> AAS) {
		this.AAS = AAS;
		typeZero = new HashMap<String, ArgumentContainer>();
		typeOne = new HashMap<String, ArgumentContainer>();
		typeTwo = new HashMap<String, ArgumentContainer>();
		determineTypesOfArgs();
		deleteNotNecessaryArgs();
	}

	/**
	 * Determines arguments which can not be part of any extension.
	 */
	private void deleteNotNecessaryArgs() {
		for (String argName : typeZero.keySet()) {
			ArgumentContainer arg = typeZero.get(argName);
			delete.putAll(arg.getAttackOn());
		}
	}
	
	/**
	 * Determines the types of arguments which are explained in the class description. 
	 */
	
	private void determineTypesOfArgs() {
		ArgumentContainer workArg;

		for (String name : AAS.keySet()) {
			workArg = AAS.get(name);
			if (workArg.getAttackOf().isEmpty()) {
				typeZero.put(workArg.name, workArg);
			} else {
				if (checkIfTypeOne(workArg)) {
					typeOne.put(workArg.name, workArg);
				} else {
					typeTwo.put(workArg.name, workArg);
				}
			}
		}
	}
	
	/**
	 * Checks if an argument is a typeOne-argument.
	 * 
	 * @param arg
	 * @return itIsTypeOne: When arg is typeOne than return true else false.
	 */
	private boolean checkIfTypeOne(ArgumentContainer arg) {
		boolean itIsTypeOne = true;
		HashMap<String, ArgumentContainer> attackOf = arg.getAttackOf();
		HashMap<String, ArgumentContainer> attackOn = arg.getAttackOn();
		for (String name : attackOf.keySet()) {
			if (attackOn.containsKey(name) != true) {
				itIsTypeOne = false;
			}
		}
		return itIsTypeOne;
	}
	
	@Override
	public HashMap<String, ArgumentContainer> getArguments() {
		return AAS;
	}

	@Override
	public HashMap<String, ArgumentContainer> getDeleted() {
		return delete;
	}

	@Override
	public HashMap<String, ArgumentContainer> getTypeZero() {
		return typeZero;
	}

	@Override
	public HashMap<String, ArgumentContainer> getTypeOne() {
		return typeOne;
	}

	@Override
	public HashMap<String, ArgumentContainer> getTypeTwo() {
		return typeTwo;
	}

}
