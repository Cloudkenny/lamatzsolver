package iccma.competition2015.candidate;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;

/**
 * This class is for the most part equal to the TgfParser from Matthias Thimm.
 * But there is one important difference, the implementation of Dung`s abstract
 * argumentation framework.
 * 
 * @authors Matthias Thimm, Nico Lamatz
 * @version 0.1
 * @see iccma.competition2015.probo.TgfParser
 */

public class TgfParserAAS {
	private HashMap<String, ArgumentContainer> arguments = new HashMap<String, ArgumentContainer>();
	private BufferedReader BR;

	public TgfParserAAS(File filePath) throws IOException {
		this.BR = new BufferedReader(new FileReader(filePath));
		initAbstractArgumentationSystem();
	}
	
	/*
	 * @see iccma.competition2015.probo.TgfParser
	 * @throws IOException
	 */
	private void initAbstractArgumentationSystem() throws IOException {

		String row;
		boolean argumentSection = true;
		ArgumentContainer recentlyAttacked = null;
		ArgumentContainer recentlyAttacker = null;
		while ((row = BR.readLine()) != null) {
			if (row.trim().equals(""))
				continue;
			if (row.trim().equals("#")) {
				argumentSection = false;
				continue;
			}
			if (argumentSection) {
				ArgumentContainer is = new ArgumentContainer(row);
				arguments.put(row, is);

			} else {
				String Attackername = row.substring(0, row.indexOf(" ")).trim();
				String Attackedname = row.substring(row.indexOf(" ") + 1,
						row.length()).trim();
				recentlyAttacked = arguments.get(Attackedname);
				recentlyAttacker = arguments.get(Attackername);
				recentlyAttacked.addAttackOf(recentlyAttacker);
				recentlyAttacker.addAttackOn(recentlyAttacked);

			}
		}
		BR.close();
	}
	
	/**
	 * Returns dungs abstract argumentation framework as a HashMap.
	 *  
	 * @return arguments: the abstract argumentation framework as a HashMap.
	 */
	public HashMap<String, ArgumentContainer> getAbstractArgumentationSystem() {
		return arguments;
	}
}
