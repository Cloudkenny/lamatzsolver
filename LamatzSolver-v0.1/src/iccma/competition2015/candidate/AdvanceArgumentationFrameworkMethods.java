package iccma.competition2015.candidate;

import java.util.HashMap;

/**
 * This interface provides methods for getting further information in Dung`s
 * abstract argumentation framework.
 * 
 * @author Nico Lamatz
 * @version 0.1
 */

public interface AdvanceArgumentationFrameworkMethods {
	
	/*Get the entire arguments of the abstract argumentation framework as a HashMap.*/
	public HashMap<String, ArgumentContainer> getArguments();

	/*Get arguments deleted by TypeZero arguments.*/
	public HashMap<String, ArgumentContainer> getDeleted();
	
	/*Get TypeZero arguments.*/
	public HashMap<String, ArgumentContainer> getTypeZero();
	
	/*Get TypeOne arguments.*/
	public HashMap<String, ArgumentContainer> getTypeOne();
	
	/*Get TypeTwo arguments.*/
	public HashMap<String, ArgumentContainer> getTypeTwo();

}
