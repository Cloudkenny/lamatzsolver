package iccma.competition2015.candidate;

import java.util.HashMap;

/**
 * This class provides a container for arguments. An argument can be stored by
 * name and attacks from both sides. Both sides means:
 * 
 * This -> another argument (attackedOn)
 * Another argument -> this (attackedOf)
 * 
 * @author Nico Lamatz
 * @version 1.0
 */

public class ArgumentContainer {
	
	/*@see class description*/
	private HashMap<String, ArgumentContainer> attackOn;
	private HashMap<String, ArgumentContainer> attackOf;
	public String name;

	public ArgumentContainer(String name) {
		this.name = name;
		this.attackOn = new HashMap<String, ArgumentContainer>();
		this.attackOf = new HashMap<String, ArgumentContainer>();
	}
	
	/**
	 * Adds an attack from this to another argument.
	 * 
	 * @param argument: Add attack from this to another argument.
	 */
	
	public void addAttackOn(ArgumentContainer argument) {
		attackOn.put(argument.name, argument);
	}

	/**
	 * Adds an attack from another argument to this.
	 * 
	 * @param argument: Add attack from another argument to this.
	 */
	
	public void addAttackOf(ArgumentContainer argument) {
		attackOf.put(argument.name, argument);
	}

	/**
	 * Get the arguments which are attacked by this argument.
	 * 
	 * @return attackOn: HashMap of the attacked arguments 
	 */
	
	public HashMap<String, ArgumentContainer> getAttackOn() {
		return attackOn;
	}
	
	/**
	 * Get the arguments which are attacking this argument.
	 * 
	 * @return attackOf: HashMap of the attacking arguments 
	 */

	public HashMap<String, ArgumentContainer> getAttackOf() {
		return attackOf;
	}

}
