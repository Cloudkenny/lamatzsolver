package iccma.competition2015.candidate;

import java.io.File;
import java.io.IOException;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import iccma.competition2015.probo.AbstractSolver;
import iccma.competition2015.probo.FileFormat;
import iccma.competition2015.probo.Problem;

/**
 * An implementation of the abstract class AbstractSolver from Mathias Thimm.
 * 
 * @author Nico Lamatz
 * @version 0.1
 * @see iccma.competition2015.probo.AbstractSolver
 */

public class LamatzSolver extends AbstractSolver {

	public static void main(String[] args) throws IOException {
		new LamatzSolver().execute(args);
	}

	@Override
	public String versionInfo() {
		return "LamatzSolver v0.1\nNico Lamatz (Lamatz.Nico@gmail.com)";
	}

	@Override
	public Collection<FileFormat> supportedFormats() {
		Collection<FileFormat> formats = new HashSet<FileFormat>();
		formats.add(FileFormat.TGF);
		return formats;
	}

	@Override
	public Collection<Problem> supportedProblems() {
		Collection<Problem> problems = new HashSet<Problem>();
		problems.add(Problem.EE_GR);
		return problems;
	}

	@Override
	public String solve(Problem problem, File input, FileFormat format,
			String additionalParameters) throws IOException,
			IllegalArgumentException {

		AdvanceAAS AAAS = null;
		String grounded = null;

		if (format == FileFormat.TGF) {
			TgfParserAAS TgfParser = new TgfParserAAS(input);
			AAAS = new AdvanceAAS(TgfParser.getAbstractArgumentationSystem());
		}
		if (problem == Problem.EE_GR) {
			GroundedExtensionFinder GEF = new GroundedExtensionFinder(
					AAAS.getTypeZero());
			HashMap<String, ArgumentContainer> groundedExt = GEF
					.getGroundedExtension();
			grounded = getHashMapToString(groundedExt);
		}
		return grounded;
	}

	private String getHashMapToString(
			HashMap<String, ArgumentContainer> groundedExt) {
		String getGrounded = "[";
		if (groundedExt.isEmpty()) {
			getGrounded = getGrounded.concat("[]");

		} else {
			HashSet<String> set = new HashSet<String>(groundedExt.keySet());
			Iterator<String> it = set.iterator();
			getGrounded = getGrounded.concat("[");
			while (it.hasNext()) {
				String current = it.next();
				if (it.hasNext()) {
					getGrounded = getGrounded.concat(current + ",");
				} else {
					getGrounded = getGrounded.concat(current);
				}
			}
			getGrounded = getGrounded.concat("]");
		}
		getGrounded = getGrounded.concat("]");
		return getGrounded;
	}

	@Override
	public void execute(String[] args) {
		// if no arguments are given just print out the version info
		if (args.length == 0) {
			System.out.println(this.versionInfo());
			return;
		}
		// for the parameter "--formats" print out the formats
		if (args[0].toLowerCase().equals("--formats")) {
			System.out.println(this.supportedFormats());
			return;
		}
		// for the parameter "--problems" print out the formats
		if (args[0].toLowerCase().equals("--problems")) {
			System.out.println(this.supportedProblems());
			return;
		}
		// otherwise parse for a problem
		String p = null, f = null, fo = null, a = null;
		for (int i = 0; i < args.length; i++) {
			if (args[i].toLowerCase().equals("-p")) {
				p = args[++i];
				continue;
			}
			if (args[i].toLowerCase().equals("-f")) {
				f = args[++i];
				continue;
			}
			if (args[i].toLowerCase().equals("-fo")) {
				fo = args[++i];
				continue;
			}
			if (args[i].toLowerCase().equals("-a")) {
				a = args[++i];
				continue;
			}
		}
		// if some parameter is missing exit with error (additional parameter is
		// optional)
		if (p == null || f == null || fo == null) {
			System.out.println("Error: unrecognized command parameters");
			return;
		}
		Problem problem = Problem.getProblem(p);
		FileFormat format = FileFormat.getFileFormat(fo);
		// check if the problem is supported
		try {
			if (!this.supportedProblems().contains(problem)) {
				System.out.println("Error: problem instance not supported");
				System.exit(1);
			}
			if (!this.supportedFormats().contains(format)) {
				System.out.println("Error: file format not supported");
				System.exit(1);
			}
			System.out.println(this.solve(problem, new File(f), format, a));
		} catch (NullPointerException e) {
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			System.out.println("Error: unforeseen exception \""
					+ e.getMessage() + "\"");
		} catch (IOException e) {
			System.out.println("Error: IO error in reading file " + f);
		}
	}

}
