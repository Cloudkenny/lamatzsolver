package iccma.competition2015.probo;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;

import net.sf.tweety.arg.dung.DungTheory;
import net.sf.tweety.arg.dung.semantics.ArgumentStatus;
import net.sf.tweety.arg.dung.semantics.Extension;
import net.sf.tweety.arg.dung.semantics.Labeling;
import net.sf.tweety.arg.dung.syntax.Argument;
import net.sf.tweety.commons.util.RandomSubsetIterator;
import net.sf.tweety.commons.util.SetTools;
import net.sf.tweety.commons.util.SubsetIterator;

/**
 * This is the main class for performing benchmarks of argumentation solvers.
 * 
 * @author Matthias Thimm
 */
public abstract class Benchmark {

	/**
	 * Solvers to be tested
	 */
	private static String[] solvers = {"solvers/tweetysolver-v1.0.6.sh"};
	
	/**
	 * The log of the benchmark.
	 */
	private static String log = "log_" + System.currentTimeMillis();
	
	/**
	 * Argumentation frameworks to be tested
	 */
	private static String[] filesTgf = {"examples/ex1.tgf", "examples/ex2.tgf", "examples/ex3.tgf"};
	private static String[] filesApx = {"examples/ex1.apx", "examples/ex2.apx", "examples/ex3.apx"};
	private static String[] filesCnf = {"examples/ex1.cnf", "examples/ex2.cnf", "examples/ex3.cnf"};
	
	/** The number of arguments tested for the problem DC. */
	private static final int DC_NUMBEROFARGUMENTS = 10;
	/** The number of arguments tested for the problem DS. */
	private static final int DS_NUMBEROFARGUMENTS = 10;
	/** The number of extensions tested for the problem DE. */
	private static final int DE_NUMBEROFEXTENSIONS = 10;
	/** The number of labelings tested for the problem DL. */
	private static final int DL_NUMBEROFLABELINGS = 10;
	
	/**
	 * TweetySolver is the reference solver that provides
	 * correct answers to queries.
	 */
	private static AbstractDungSolver referenceSolver = new TweetySolver();
	
	/**
	 * Executes the given command on the commandline and returns the output.
	 * @param commandline some command
	 * @return the output of the execution
	 * @throws IOException
	 * @throws InterruptedException 
	 */
	private static String invokeExecutable(String commandline) throws IOException, InterruptedException{
		Process child = Runtime.getRuntime().exec(commandline);
		child.waitFor();
		String output = "";
		BufferedReader reader = new BufferedReader(new InputStreamReader(child.getInputStream()));
		String line = "";			
		while((line = reader.readLine())!= null) {
			output += line + "\n";
		}
		reader.close();
		// check for errors
		reader = new BufferedReader(new InputStreamReader(child.getErrorStream())); 
		line = "";		
		String error = "";
		while((line = reader.readLine())!= null) {
			error += line + "\n";
		}
		reader.close();
		child.destroy();
		error.trim();
		if(!error.equals(""))
			throw new IOException(error);
		return output;
	}
	
	/**
	 * Returns a map that maps all solvers to their corresponding supported problems.
	 * @return a map that maps all solvers to their corresponding supported problems.
	 * @throws IOException 
	 * @throws InterruptedException 
	 */
	private static Map<String,Collection<Problem>> supportedProblems() throws IOException, InterruptedException{
		Map<String,Collection<Problem>> map = new HashMap<String,Collection<Problem>>(); 
		for(String solver: solvers){
			String output = Benchmark.invokeExecutable(solver + " --problems");
			map.put(solver, Problem.getProblems(output));
		}
		return map;
	}

	/**
	 * Returns a map that maps all solvers to one of their supported file formats.
	 * @return a map that maps all solvers to one of their supported file formats.
	 * @throws IOException 
	 * @throws InterruptedException 
	 */
	private static Map<String,FileFormat> supportedFormat() throws IOException, InterruptedException{
		Map<String,FileFormat> map = new HashMap<String,FileFormat>(); 
		for(String solver: solvers){
			String output = Benchmark.invokeExecutable(solver + " --formats");			
			map.put(solver, FileFormat.getFileFormats(output).iterator().next());
		}
		return map;
	}
	
	/**
	 * Returns the file of the given index and format.
	 * @param fo some file format.
	 * @param idx some index.
	 * @return a file
	 */
	private static String getFileOfFormat(FileFormat fo, int idx){
		if(fo.equals(FileFormat.APX))
			return filesApx[idx];
		if(fo.equals(FileFormat.TGF))
			return filesTgf[idx];
		if(fo.equals(FileFormat.CNF))
			return filesCnf[idx];
		throw new RuntimeException("Unknown file format");
	}
		
	/**
	 * Logs the given message to standard out and to
	 * the log file.
	 * @param str some string
	 * @throws IOException 
	 */
	private static void log(String str) throws IOException{
		PrintWriter writer = new PrintWriter(new BufferedWriter(new FileWriter(Benchmark.log, true)));
		writer.println(str);
		writer.close();
		System.out.println(str);
	}
	
	/**
	 * Main benchmark method
	 * @param args no arguments needed
	 * @throws IOException 
	 * @throws InterruptedException 
	 */
	public static void main(String[] args) throws IOException, InterruptedException{
		// get from all solvers their supported problems
		Map<String,Collection<Problem>> supportedProblems = Benchmark.supportedProblems();
		// get from all solvers one of their supported formats
		Map<String,FileFormat> supportedFormat = Benchmark.supportedFormat();
		// Iterate over all possible problems
		for(Problem problem: Problem.values()){
			Benchmark.log("=========================================================================================================================================================");
			Benchmark.log("Problem: " + problem.toString());
			Benchmark.log("Solvers supporting the problem:");
			Collection<String> supportingSolvers = new HashSet<String>();
			for(String solver: solvers){
				if(supportedProblems.get(solver).contains(problem)){
					supportingSolvers.add(solver);
					Benchmark.log("\t- " + solver);
				}
			}
			Parser tgfParser = new TgfParser();
			String output;
			Benchmark.log("");
			Benchmark.log("Benchmark run:");
			long time;
			String cmd;
			for(int i = 0; i < filesTgf.length; i++){				
				// for problems without further parameters
				// just compare output with output of reference solver
				DungTheory aaf = tgfParser.parse(new File(filesTgf[i]));			
				if(problem.subProblem().equals(Problem.SubProblem.EC)){
					Extension solution = new Extension(referenceSolver.solveEC(problem.semantics(), aaf));					
					for(String solver: solvers){
						cmd = solver + " -p " + problem.toString() + " -f " + Benchmark.getFileOfFormat(supportedFormat.get(solver), i) + " -fo " + supportedFormat.get(solver).toString();
						time = System.currentTimeMillis();
						output = Benchmark.invokeExecutable(cmd);
						time = System.currentTimeMillis() - time;
						Collection<Argument> s = Parser.parseArgumentList(output);
						Benchmark.log("I;" + cmd + ";" + (s.equals(solution)? "correct": "incorrect") + ";" + time);						
					}
				}
				if(problem.subProblem().equals(Problem.SubProblem.EE)){
					Collection<Collection<Argument>> solution = referenceSolver.solveEE(problem.semantics(), aaf);
					for(String solver: solvers){
						cmd = solver + " -p " + problem.toString() + " -f " + Benchmark.getFileOfFormat(supportedFormat.get(solver), i) + " -fo " + supportedFormat.get(solver).toString();
						time = System.currentTimeMillis();
						output = Benchmark.invokeExecutable(cmd);
						time = System.currentTimeMillis() - time;
						Collection<Collection<Argument>> s = Parser.parseExtensionList(output);
						Benchmark.log("I;" + cmd + ";" + (s.equals(solution)? "correct": "incorrect") + ";" + time);						
					}
				}
				if(problem.subProblem().equals(Problem.SubProblem.SE)){
					Collection<Collection<Argument>> solution = referenceSolver.solveEE(problem.semantics(), aaf);
					for(String solver: solvers){
						cmd = solver + " -p " + problem.toString() + " -f " + Benchmark.getFileOfFormat(supportedFormat.get(solver), i) + " -fo " + supportedFormat.get(solver).toString();
						time = System.currentTimeMillis();
						output = Benchmark.invokeExecutable(cmd);
						time = System.currentTimeMillis() - time;
						// check for non-existence of extensions
						if (solution == null){
							Benchmark.log("I;" + cmd + ";" + output.toLowerCase().equals("no") + ";" + time);							
						}else{
							Collection<Argument> s = Parser.parseArgumentList(output);
							Benchmark.log("I;" + cmd + ";" + (solution.contains(s)? "correct": "incorrect") + ";" + time);
						}
					}
				}
				if(problem.subProblem().equals(Problem.SubProblem.EL)){
					Collection<Labeling> solution = referenceSolver.solveEL(problem.semantics(), aaf);
					for(String solver: solvers){
						cmd = solver + " -p " + problem.toString() + " -f " + Benchmark.getFileOfFormat(supportedFormat.get(solver), i) + " -fo " + supportedFormat.get(solver).toString();
						time = System.currentTimeMillis();
						output = Benchmark.invokeExecutable(cmd);
						time = System.currentTimeMillis() - time;
						Collection<Labeling> s = Parser.parseLabelingList(output);
						Benchmark.log("I;" + cmd + ";" + (s.equals(solution)? "correct": "incorrect") + ";" + time);
					}
				}
				if(problem.subProblem().equals(Problem.SubProblem.ES)){				
					Extension solution = new Extension(referenceSolver.solveES(problem.semantics(), aaf));
					for(String solver: solvers){
						cmd = solver + " -p " + problem.toString() + " -f " + Benchmark.getFileOfFormat(supportedFormat.get(solver), i) + " -fo " + supportedFormat.get(solver).toString();
						time = System.currentTimeMillis();
						output = Benchmark.invokeExecutable(cmd);
						time = System.currentTimeMillis() - time;
						Extension s = Parser.parseArgumentList(output);
						Benchmark.log("I;" + cmd + ";" + (s.equals(solution)? "correct": "incorrect") + ";" + time);
					}
				}
				if(problem.subProblem().equals(Problem.SubProblem.DX)){
					boolean solution = referenceSolver.solveDX(problem.semantics(), aaf);
					for(String solver: solvers){
						cmd = solver + " -p " + problem.toString() + " -f " + Benchmark.getFileOfFormat(supportedFormat.get(solver), i) + " -fo " + supportedFormat.get(solver).toString();
						time = System.currentTimeMillis();
						output = Benchmark.invokeExecutable(cmd);
						time = System.currentTimeMillis() - time;
						boolean s = Parser.parseBoolean(output);
						Benchmark.log("I;" + cmd + ";" + (s == solution? "correct": "incorrect") + ";" + time);
					}
				}
				if(problem.subProblem().equals(Problem.SubProblem.DN)){
					boolean solution = referenceSolver.solveDN(problem.semantics(), aaf);
					for(String solver: solvers){
						cmd = solver + " -p " + problem.toString() + " -f " + Benchmark.getFileOfFormat(supportedFormat.get(solver), i) + " -fo " + supportedFormat.get(solver).toString();
						time = System.currentTimeMillis();
						output = Benchmark.invokeExecutable(cmd);
						time = System.currentTimeMillis() - time;
						boolean s = Parser.parseBoolean(output);
						Benchmark.log("I;" + cmd + ";" + (s == solution? "correct": "incorrect") + ";" + time);
					}
				}
				if(problem.subProblem().equals(Problem.SubProblem.DC)){
					// DC is about checking whether a given argument is credulously accepted
					// for that we randomly pick #DC_NUMBEROFARGUMENTS of the arguments of the argumentation framework					
					SetTools<Argument> setTools = new SetTools<Argument>();
					List<Set<Argument>> subsets = new ArrayList<Set<Argument>>(setTools.subsets(aaf, Math.min(aaf.size(), Benchmark.DC_NUMBEROFARGUMENTS)));
					Collection<Argument> toBeTested = subsets.get(new Random().nextInt(subsets.size()));					
					Collection<Argument> solution = referenceSolver.solveEC(problem.semantics(), aaf);
					for(String solver: solvers){
						for(Argument a: toBeTested){
							cmd = solver + " -p " + problem.toString() + " -f " + Benchmark.getFileOfFormat(supportedFormat.get(solver), i) + " -fo " + supportedFormat.get(solver).toString() + " -a " + a.getName();
							time = System.currentTimeMillis();
							output = Benchmark.invokeExecutable(cmd);
							time = System.currentTimeMillis() - time;
							boolean s = Parser.parseBoolean(output);
							Benchmark.log("I;" + cmd + ";" + ((s && solution.contains(a) || (!s && !solution.contains(a)))? "correct": "incorrect") + ";" + time);
						}
					}
				}
				if(problem.subProblem().equals(Problem.SubProblem.DS)){
					// DS is about checking whether a given argument is skeptically accepted
					// for that we randomly pick #DS_NUMBEROFARGUMENTS of the arguments of the argumentation framework					
					SetTools<Argument> setTools = new SetTools<Argument>();
					List<Set<Argument>> subsets = new ArrayList<Set<Argument>>(setTools.subsets(aaf, Math.min(aaf.size(), Benchmark.DS_NUMBEROFARGUMENTS)));
					Collection<Argument> toBeTested = subsets.get(new Random().nextInt(subsets.size()));					
					Collection<Argument> solution = referenceSolver.solveES(problem.semantics(), aaf);
					for(String solver: solvers){
						for(Argument a: toBeTested){
							cmd = solver + " -p " + problem.toString() + " -f " + Benchmark.getFileOfFormat(supportedFormat.get(solver), i) + " -fo " + supportedFormat.get(solver).toString() + " -a " + a.getName();
							time = System.currentTimeMillis();
							output = Benchmark.invokeExecutable(cmd);
							time = System.currentTimeMillis() - time;
							boolean s = Parser.parseBoolean(output);
							Benchmark.log("I;" + cmd + ";" + ((s && solution.contains(a) || (!s && !solution.contains(a)))? "correct": "incorrect") + ";" + time);
						}
					}
				}
				if(problem.subProblem().equals(Problem.SubProblem.DE)){
					// DE is about checking whether a given set of arguments is an extension
					// for that we 0.5 #DE_NUMBEROFEXTENSIONS from the reference solver and generate
					// 0.5 #DE_NUMBEROFEXTENSIONS random argument sets 
					Collection<Collection<Argument>> toBeTested = new HashSet<Collection<Argument>>();
					Collection<Collection<Argument>> solution = referenceSolver.solveEE(problem.semantics(), aaf);
					SetTools<Collection<Argument>> setTools = new SetTools<Collection<Argument>>();
					List<Set<Collection<Argument>>> subsets = new ArrayList<Set<Collection<Argument>>>(setTools.subsets(solution, Math.min(solution.size(), Benchmark.DE_NUMBEROFEXTENSIONS/2)));
					toBeTested.addAll(subsets.get(new Random().nextInt(subsets.size())));
					SubsetIterator<Argument> it = new RandomSubsetIterator<Argument>(new HashSet<Argument>(aaf),false);
					while(toBeTested.size() < Benchmark.DE_NUMBEROFEXTENSIONS && toBeTested.size() < Math.pow(2, aaf.size())){
						toBeTested.add(it.next());
					}
					for(String solver: solvers){
						for(Collection<Argument> arguments: toBeTested){
							Extension e = new Extension(arguments);
							cmd = solver + " -p " + problem.toString() + " -f " + Benchmark.getFileOfFormat(supportedFormat.get(solver), i) + " -fo " + supportedFormat.get(solver).toString() + " -a " + Writer.writeArguments(arguments);
							time = System.currentTimeMillis();
							output = Benchmark.invokeExecutable(cmd);
							time = System.currentTimeMillis() - time;
							boolean s = Parser.parseBoolean(output);
							Benchmark.log("I;" + cmd + ";" + ((s && solution.contains(e) || (!s && !solution.contains(e)))? "correct": "incorrect") + ";" + time);							
						}
					}					
				}
				if(problem.subProblem().equals(Problem.SubProblem.DL)){
					// DL is about checking whether a given labeling is valid labeling
					// for that we 0.5 #DL_NUMBEROFLABELINGS from the reference solver and generate
					// 0.5 #DE_NUMBEROFLABELINGS random labelings 
					Collection<Labeling> toBeTested = new HashSet<Labeling>();
					Collection<Labeling> solution = referenceSolver.solveEL(problem.semantics(), aaf);
					SetTools<Labeling> setTools = new SetTools<Labeling>();
					List<Set<Labeling>> subsets = new ArrayList<Set<Labeling>>(setTools.subsets(solution, Math.min(solution.size(), Benchmark.DL_NUMBEROFLABELINGS/2)));
					toBeTested.addAll(subsets.get(new Random().nextInt(subsets.size())));
					Random rand = new Random();
					while(toBeTested.size() < Benchmark.DL_NUMBEROFLABELINGS && toBeTested.size() < Math.pow(2, aaf.size())){
						Labeling l = new Labeling();
						for(Argument a: aaf){
							int r = rand.nextInt(3);
							if(r == 0) l.put(a, ArgumentStatus.IN);
							else if (r==1) l.put(a, ArgumentStatus.OUT);
							else l.put(a, ArgumentStatus.UNDECIDED);								
						}
						toBeTested.add(l);
					}
					for(String solver: solvers){
						for(Labeling lab: toBeTested){
							cmd = solver + " -p " + problem.toString() + " -f " + Benchmark.getFileOfFormat(supportedFormat.get(solver), i) + " -fo " + supportedFormat.get(solver).toString() + " -a " + Writer.writeLabeling(lab);
							time = System.currentTimeMillis();
							output = Benchmark.invokeExecutable(cmd);
							time = System.currentTimeMillis() - time;
							boolean s = Parser.parseBoolean(output);
							Benchmark.log("I;" + cmd + ";" + ((s && solution.contains(lab) || (!s && !solution.contains(lab)))? "correct": "incorrect") + ";" + time);
						}
					}	
				}				
			}
			Benchmark.log("=========================================================================================================================================================");
		}		
	}
}
